# This Makefile is in the public domain

.PHONY: graphics

SUBDIRS := php/doc python/doc

all: $(SUBDIRS)

$(SUBDIRS): graphics
	@cp -t $@ common/graphics/arch.png
	@$(MAKE) -C $@

graphics:
	@cd common/graphics; dot -Tpdf arch.dot > arch.pdf
	@cd common/graphics; dot -Tpdf arch_nobo.dot > arch_nobo.pdf
	@cd common/graphics; dot -Tjpg arch.dot > arch.jpg
	@cd common/graphics; dot -Tjpg arch_nobo.dot > arch_nobo.jpg
