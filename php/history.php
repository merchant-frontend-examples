<?php
  
  include "helpers.php";
  include "backend.php";
  include "error.php";

  // Just relay the request we got from the JavaScript
  $response = get_to_backend("/history", $_GET);

  if (200 != $response["status_code"]){
    echo build_error($response,
                     "Backend error",
                     $response["status_code"]);
    return;
  }

  // Give the response "verbatim" back.
  echo $response["body"];
?>
