@c Section describing the format of Taler contracts/proposals in detail

@node The Taler proposal format
@section The Taler proposal format
@cindex contract

A Taler proposal can specify many details about the transaction.
This section describes each of the fields in depth.

@table @var
@item amount
@cindex amount
Specifies the total amount to be paid to the merchant by the customer.
The amount is broken up into a @var{value}, a @var{fraction}
(100.000.000 @var{fraction} units correspond to one @var{value}) and
the @var{currency}.  For example, @code{EUR 1.50} would be represented
as the tuple @code{value = 1, fraction = 50000000, currency = "EUR"}.

@item max_fee
@cindex fees
@cindex maximum deposit fee
This is the maximum total amount of deposit fees that the merchant is
willing to pay.  If the deposit fees for the coins exceed this amount,
the customer has to include it in the payment total.  The fee is
specified using the same triplet used for @var{amount}.


@item max_wire_fee
@cindex fees
@cindex maximum wire fee
Maximum wire fee accepted by the merchant (customer share to be
divided by the 'wire_fee_amortization' factor, and further reduced
if deposit fees are below 'max_fee').  Default if missing is zero.


@item wire_fee_amortization
@cindex fees
@cindex maximum fee amortization
Over how many customer transactions does the merchant expect to
amortize wire fees on average?  If the exchange's wire fee is
above 'max_wire_fee', the difference is divided by this number
to compute the expected customer's contribution to the wire fee.
The customer's contribution may further be reduced by the difference
between the 'max_fee' and the sum of the actual deposit fees.
Optional, default value if missing is 1.  0 and negative values are
invalid and also interpreted as 1.

@item pay_url
@cindex pay_url
Which URL accepts payments. This is the URL where the wallet will POST
coins.

@item fulfillment_url
@cindex fulfillment URL
Which URL should the wallet go to for obtaining the fulfillment,
for example the HTML or PDF of an article that was bought, or an
order tracking system for shipments, or a simple human-readable
Web page indicating the status of the contract.

@item order_id
@cindex order ID
Alphanumeric identifier, freely definable by the merchant.
Used by the merchant to uniquely identify the transaction.

@item summary
@cindex summary
Short, human-readable summary of the contract. To be used when
displaying the contract in just one line, for example in the
transaction history of the customer.

@item timestamp
Time at which the offer was generated.
@c FIXME: describe time format in detail here

@item pay_deadline
@cindex payment deadline
Timestamp of the time by which the merchant wants the exchange
to definitively wire the money due from this contract.  Once
this deadline expires, the exchange will aggregate all
deposits where the contracts are past the @var{refund_deadline}
and execute one large wire payment for them.  Amounts will be
rounded down to the wire transfer unit; if the total amount is
still below the wire transfer unit, it will not be disbursed.

@item refund_deadline
@cindex refund deadline
Timestamp until which the merchant willing (and able) to give refunds
for the contract using Taler.  Note that the Taler exchange will hold
the payment in escrow at least until this deadline.  Until this time,
the merchant will be able to sign a message to trigger a refund to the
customer.  After this time, it will no longer be possible to refund
the customer.  Must be smaller than the @var{pay_deadline}.

@item products
@cindex product description
Array of products that are being sold to the customer.  Each
entry contains a tuple with the following values:

@table @var
@item description
Description of the product.
@item quantity
Quantity of the items to be shipped. May specify a unit (@code{1 kg})
or just the count.
@item price
Price for @var{quantity} units of this product shipped to the
given @var{delivery_location}. Note that usually the sum of all
of the prices should add up to the total amount of the contract,
but it may be different due to discounts or because individual
prices are unavailable.
@item product_id
Unique ID of the product in the merchant's catalog.  Can generally
be chosen freely as it only has meaning for the merchant, but
should be a number in the range @math{[0,2^{51})}.
@item taxes
Map of applicable taxes to be paid by the merchant.  The label is the
name of the tax, i.e. @var{VAT}, @var{sales tax} or @var{income tax},
and the value is the applicable tax amount.  Note that arbitrary
labels are permitted, as long as they are used to identify the
applicable tax regime.  Details may be specified by the regulator.
This is used to declare to the customer which taxes the merchant
intends to pay, and can be used by the customer as a receipt.
@c FIXME: a receipt not including the item's price?
The information is also likely to be used by tax audits of the merchant.
@item delivery_date
Time by which the product is to be delivered to the
@var{delivery_location}.
@item delivery_location
This should give a label in the @var{locations} map, specifying
where the item is to be delivered.
@end table
Values can be omitted if they are not applicable. For example, if a
purchase is about a bundle of products that have no individual prices
or product IDs, the @var{product_id} or @var{price} may not be
specified in the contract.  Similarly, for virtual products delivered
directly via the fulfillment URI, there is no delivery location.

@item merchant
@table @var
@item address
This should give a label in the @var{locations} map, specifying
where the merchant is located.
@item name
This should give a human-readable name for the merchant's business.
@item jurisdiction
This should give a label in the @var{locations} map, specifying
the jurisdiction under which this contract is to be arbitrated.
@end table

@item locations
@cindex location
Associative map of locations used in the contract. Labels for
locations in this map can be freely chosen and used whenever
a location is required in other parts of the contract.  This way,
if the same location is required many times (such as the business
address of the customer or the merchant), it only needs to be
listed (and transmitted) once, and can otherwise be referred to
via the label.  A non-exhaustive list of location attributes
is the following:
@table @var
@item country
Name of the country for delivery, as found on a postal package, i.e. ``France''.
@item state
Name of the state for delivery, as found on a postal package, i.e. ``NY''.
@item region
Name of the region for delivery, as found on a postal package.
@item province
Name of the province for delivery, as found on a postal package.
@item city
Name of the city for delivery, as found on a postal package.
@item ZIP code
ZIP code for delivery, as found on a postal package.
@item street
Street name for delivery, as found on a postal package.
@item street number
Street number (number of the house) for delivery, as found on a postal package.
@item name receiver name for delivery, either business or person name.

@end table

Note that locations are not required to specify all of these fields,
and it is also allowed to have additional fields.  Contract renderers
must render at least the fields listed above, and should render fields
that they do not understand as a key-value list.

@end table
