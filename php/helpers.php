<?php
  // This file is in the public domain.

  /**
   * If '$arr[$idx]' exists, return it. Otherwise
   * set '$arr[$idx]' to '$default' and then return it
   * as well.
   */
  function &pull(&$arr, $idx, $default) {
    if (!isset($arr[$idx])) {
      $arr[$idx] = $default;
    }
    return $arr[$idx];
  }

  /**
   * Concatenates '$base' and '$path'.  Typically used
   * to add the path (represented by '$path') to a base URL
   * (represented by '$base').
   */
  function url_join($base, $path) {
    // Please note that this simplistic way of joining URLs is
    // to avoiding using pecl_http (painful installation!) and
    // to NOT bloat too much tutorial code.
    return $base . $path;
  }

  /**
   * Construct a URL having the host where the script is
   * running as the base URL, and concatenating '$path' to
   * it.  NOTE: $path must have a leading '/'.
   */
  function url_rel($path){
    // Make sure 'REQUEST_SCHEME' is http/https, as in some setups it may
    // be "HTTP/1.1".
    return $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$path;
  }

  /**
   * Take date in Taler format, as "/Date(timestamp)/", and
   * convert it in a human-readable string.
   */
  function get_pretty_date($taler_date){
    $match = array();
    $timestamp = preg_match('/\/Date\(([0-9]+)\)\//', $taler_date, $match);
    $date = new DateTime();
    $date->setTimestamp(intval($match[1]));
    return $date->format('d/M/Y H:i:s');
  }

  /**
   * Takes amount object in Taler format, and converts it
   * in a human-readable string.  NOTE: Taler amounts objects
   * come from JSON of the form:
   * '{"value" x, "fraction": y, "currency": "CUR"}'
   */
  function get_amount($taler_amount){
    $PRECISION = 100000000;
    $fraction = $taler_amount->fraction / $PRECISION;
    $number = $taler_amount->value + $fraction;
    return sprintf("%s %s", $number, $taler_amount->currency);
  }

?>
