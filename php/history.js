var FRACTION = 100000000;

// Stringify amounts. Take a {value: x, fraction: y, currency: "Z"}
// and return a "a.b Z" form.
function parse_amount(amount){
  var v = amount.value + (amount.fraction/FRACTION);
  return v + " " + amount.currency;
}

// Parse Taler date ("/Date(TIMESTAMP)/") string and
// return a JavaScript Date object.
function get_date(date){
  var split = date.match(/Date\((.*)\)/);
  var seconds;
  if(isNaN(seconds = Number(split[1]))){
    console.error("Malformed date gotten from backend");
    return;
  }
  console.log("Extracting timestamp", split[1]);
  var d = new Date(seconds * 1000);
  return d;
}


// Perform the call to /history.php?instance=tutorial.
// It also takes care of cleaning/filling the table showing
// the results.
function submit_history(){

  // Clean the table showing old results
  var table = document.getElementById("history");
  /* We don't want to kill the first child */
  for (var i = 2; i < table.childNodes.length; i++)
    table.removeChild(table.childNodes[i]);
  var req = new XMLHttpRequest();
  var get_column = function(value){
    var column = document.createElement("td");
    column.textContent = value;
    return column;
  };
  var on_error = function(){
    table.innerHTML = "<p>Could not get transactions list from server</p>"
  };
  req.open("GET", "/history.php?instance=tutorial", true);
  req.onload = function(){
    if(req.readyState == 4 && req.status == 200){
      console.log("Got history:", req.responseText);
      var history = JSON.parse(req.responseText); 
      if(!history)
        console.log("Got invalid JSON");
      if(0 == history.length){
        table.innerHTML = "<p>No transaction was that young!</p>"; 
      }
      // Fill the table with fresh results
      for (var i=0; i<history.length; i++){
	var entry = history[i];
        var tr = document.createElement("tr");
        tr.appendChild(get_column(entry.order_id));
	var date = get_date(entry.timestamp);
	tr.appendChild(get_column(date.toLocaleDateString()));
	tr.appendChild(get_column(parse_amount(entry.amount)))
	table.appendChild(tr);
      }
      table.style.visibility = "";
    }
    else{
      on_error();
    }
  };
  req.send();
}
