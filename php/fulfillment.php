<?php
  // This file is in the public domain.

  include 'helpers.php';

  session_start();

  if(pull($_SESSION, 'paid', false)){
    echo sprintf("<p>Thanks for your donation!</p>
                  <br><p>The order ID is: <b>%s</b>; use it to
                  <a href=\"backoffice.html\">track</a> your money,
                  or make <a href=\"/\">another donation!</a></p>",
                  $_SESSION['order_id']);
    session_destroy();
    return;
  }

  // The user needs to pay, instruct the wallet to send the payment.
  http_response_code(402);
  header('X-Taler-Contract-Url: ' . url_rel('/generate-order.php'));
  header('X-Taler-Contract-Query: ' . "fulfillment_url");
  header('X-Taler-Offer-Url: ' . url_rel('/donate.php'));
  return;
?>
