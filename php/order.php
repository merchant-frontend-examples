<?php
  // This file is in the public domain.

  include_once 'config.php';
  include_once 'helpers.php';

  function make_order($nonce,
                      $order_id,
                      $now){
    $order
      = array(
        'nonce' => $nonce,
        'amount' =>
          array('value' => 0,
    	        'fraction' => 10000000,
                'currency' => $GLOBALS['CURRENCY']),
    	'max_fee' =>
  	  array('value' => 0,
    	        'fraction' => 5000000,
    	        'currency' => $GLOBALS['CURRENCY']),
        'products' =>
          array(array('description' =>
                         "Donation to charity program",
    		      'quantity' => 1,
    		      'price' =>
  		         array ('value' => 0,
    			        'fraction' => 10000000,
                                'currency' => $GLOBALS['CURRENCY']),
    		      'product_id' => 0,
    		      'taxes' =>
                         array(),
    		      'delivery_date' =>
                         "/Date(" . $now->getTimestamp() . ")/",
    		      'delivery_location' =>
                         'LNAME1'
                     )
               ),
        'summary' =>
          "Personal donation to charity program",
        'order_id' => $order_id,
    	'timestamp' =>
          "/Date(" . $now->getTimestamp() . ")/",
  	'fulfillment_url' =>
          url_rel("/fulfillment.php?order_id=$order_id"),
  	'pay_url' =>
          url_rel("/pay.php"),
    	'refund_deadline' =>
  	  "/Date(" . $now->getTimestamp() . ")/",
    	'pay_deadline' =>
          "/Date(" . $now->add(new DateInterval('P2W'))->getTimestamp() . ")/",
    	'merchant' =>
  	  array('address' =>
                   'LNAME2',
                'instance' => "tutorial",
    		'name' =>
                    "Charity donation shop",
    		'jurisdiction' =>
                    'LNAME2'),
        'locations' =>
  	  array ('LNAME1' =>
  		    array ('country' => 'Test Country 1',
    			   'city' => 'Test City 1',
    			   'state' => 'Test State 1',
    			   'region' => 'Test Region 1',
    			   'province' => 'Test Province 1',
    			   'ZIP code' => 49081,
    			   'street' => 'test street 1',
    			   'street number' => 201),
    		 'LNAME2' =>
  		    array ('country' => 'Test Country 2',
    		           'city' => 'Test City 2',
    		           'state' => 'Test State 2',
    		           'region' => 'Test Region 2',
    		           'province' => 'Test Province 2',
    		           'ZIP code' => 49082,
    		           'street' => 'test street 2',
    		           'street number' => 202)
                ));
    return array ('order' => $order);
  }
?>
