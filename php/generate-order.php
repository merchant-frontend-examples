<?php
  // This file is in the public domain.

  include 'order.php';
  include 'backend.php';
  include 'error.php';

  $order_id = "tutorial-" . dechex(rand(0,99999999)) . date("-H_i_s");
  session_start();
  $_SESSION["order_id"] = $order_id;
  if(!isset($_GET["nonce"]))
    return build_error(array("body" => null),
                       "no nonce given",
                       400);
  $order = make_order($_GET["nonce"],
                      strval($order_id),
                      new DateTime('now'));
  // Here the frontend POSTs the proposal to the backend
  $response = post_to_backend("/proposal", $order);
  // We always return verbatim what the backend returned
  http_response_code($response["status_code"]);
  if (200 != $response["status_code"]) {
    echo build_error($response,
                     "Failed to generate proposal",
                     $response['status_code']);
    return;
  }
  echo $response["body"];
?>
