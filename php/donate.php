<?php
  // This file is in the public domain.

  // Next two lines offer Taler payment option for Taler wallets:
  http_response_code(402); // 402: Payment required
  header ('X-Taler-Contract-Url: /generate-order.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Select payment method</title>
  </head>
  <body>
    Here you should put the HTML for the non-Taler (credit card) payment.
  </body>
</html>
