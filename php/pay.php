<?php
  // This file is in the public domain.

  include "backend.php";
  include "error.php";

  session_start();
  if(!isset($_SESSION["paid"])){
    echo "<p>No session active. Aborting.</p>";
    return;
  }
  // Get coins.
  $body = json_decode(file_get_contents("php://input"));

  $response = post_to_backend("/pay", $body);
  $body = json_decode($response["body"]);
  $_SESSION["order_id"] = $body->contract_terms->order_id;
  http_response_code($response["status_code"]);
  header("Content-Type: application/json");

  if (200 != $response["status_code"]){
    echo build_error($response,
                     "Could not send payment to backend",
                     $response["status_code"]);
    return;
  }
  // Payment went through!
  $_SESSION["paid"] = true;
  echo json_encode($body);
  return;
?>
