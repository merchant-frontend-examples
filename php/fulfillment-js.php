<?php
  // This file is in the public domain.

  include 'contract.php';
  include 'backend.php';
  include 'error.php';

  session_start();

  if(pull($_SESSION, 'paid', false)){
    echo sprintf("<p>Thanks for your donation!</p>
                  <br>
                  <p>The transaction ID was: %s; use it to
                  track your money.</p>",
                  $_SESSION['transaction_id']);
    return;
  }

  $_SESSION['transaction_id'] = $_GET['transaction_id'];

  $now = new DateTime();
  $now->setTimestamp(intval($_GET["timestamp"]));

  $rec_proposal = make_contract(intval($_GET['transaction_id']), $now);
  $response = post_to_backend("/contract", $rec_proposal);
  http_response_code($response["status_code"]);
  if (200 != $response["status_code"]) {
    echo build_error($response,
                     "Failed to reconstruct the contract",
                     $response['code']);
    return;
  }

  // Import the script
  echo "<script src=\"/taler-wallet-lib.js\" type=\"application/javascript\"></script>";
  // Invoke executePayment(). Note that the returned status code
  // does not matter now, so 200 OK is fine.
  $body = json_decode($response['body']);
  echo sprintf("<script>taler.executePayment(\"%s\", \"%s\", \"%s\");</script>",
               $body->H_contract,
               url_rel("/pay.php"),
               url_rel("/generate-contract.php"));
  return;
?>
