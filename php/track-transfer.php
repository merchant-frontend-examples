<?php
  // This file is in the public domain.

  include 'error.php';
  include 'backend.php';


  $response = get_to_backend("/track/transfer", $_GET);

  if (!in_array($response["status_code"], array(200, 424))){
    echo build_error($response,
                     "Backend error",
                     $response["status_code"]);
    return;
  }

  // Render HTML
  http_response_code($response["status_code"]);
  if (424 == $response["status_code"]){
    $body = json_decode($response["body"]);
    echo sprintf("<p>The backend detected that the amount wire
                 transferred by the exchange for coin '%s', differs
                 from the coin's original amount.</p>",
                 $body->coin_pub);
    return;
  }

  $json_response = json_decode($response["body"]);
  $pretty_date = get_pretty_date($json_response->execution_time);
  $amount = get_amount($json_response->total);
  echo "<p>$amount transferred on $pretty_date. The list of involved
       transactions is shown below:</p>";
  echo "<ul>";

  foreach ($json_response->deposits_sums as $entry){
    echo sprintf("<li>Order id: %s", $entry->order_id);
  }
  echo "</ul>";
?>
