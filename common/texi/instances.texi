@c Section from ``advanced topics'' introducing the concept of instances

@node Instances
@section Instances
@cindex instances

Taler's design allows a single backend to manage multiple frontends.
In other words, we might have multiple shops relying on the same backend.
As of terminology, we call @emph{instance} any of the frontends accounted
by the same backend.

The backend's RESTful API allows frontends to specify which instance they are.
However, this specification is optional, and a ``default'' instance will be
used whenever the frontend does not specify one.

Please note that in this stage of development, the backend's REST call @code{/history}
returns records for @emph{any} instance.  The rationale behind is to allow grouping
``public'' business entities under the same backend.

This way, a single frontend can expose multiple donation buttons for multiple
receivers, and still operate against one backend. So in this scenario, there is no
harm if the operator of instance `a' sees history entries related to instance `b'.

See @code{https://donations.demo.taler.net/}, which uses this functionality.
