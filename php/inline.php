<?php
  // This file is in the public domain.

  include "order.php";

  $order_id = rand(1,90000); // simplified, do not do this!
  $order = make_order($order_id, new DateTime("now"));

  $response = post_to_backend("/proposal", $order);
  $ret = $response["body"];

  if (200 != $response["status_code"]) {
    $ret =  build_error($response,
                        "Failed to generate proposal",
                        $response["status_code"]);
  }

  http_response_code(402); // Payment required
  header ("X-Taler-Proposal: $ret"); // Inline proposal
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Select payment method</title>
  </head>
  <body>
    Here you should put the HTML for the non-Taler (credit card) payment.
  </body>
</html>
