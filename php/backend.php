<?php
  // This file is in the public domain.

  include_once 'config.php';
  include_once 'helpers.php';

  /**
   * 'body' is an object, representing the JSON to POST. NOTE: we do NOT
   * rely on a more structured way of doing HTTP, like the one offered by
   * pecl_http, as its installation was NOT always straightforward.
   */
  function post_to_backend($backend_uri, $body){
    $json = json_encode($body);
    $c = curl_init(url_join ($GLOBALS['BACKEND'], $backend_uri));
    $options = array(CURLOPT_RETURNTRANSFER => true,
                     CURLOPT_CUSTOMREQUEST => "POST",
                     CURLOPT_POSTFIELDS => $json,
                     CURLOPT_HTTPHEADER =>
                       array('Content-Type: application/json'));
    curl_setopt_array($c, $options);
    $r = curl_exec($c);
    return array("status_code" => curl_getinfo($c, CURLINFO_HTTP_CODE),
                 "body" => $r);
  }

  /**
   * Sends a GET request to the backend.
   */
  function get_to_backend($backend_url, $args){
    $path = sprintf("%s?%s", $backend_url, http_build_query($args));
    $c = curl_init(url_join($GLOBALS['BACKEND'], $path));

    $options = array(CURLOPT_RETURNTRANSFER => true,
                     CURLOPT_CUSTOMREQUEST => "GET");
    curl_setopt_array($c, $options);
    $r = curl_exec($c);
    return array("status_code" => curl_getinfo($c, CURLINFO_HTTP_CODE),
                 "body" => $r);
  }
?>
