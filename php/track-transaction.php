<?php
  // This file is in the public domain.

  include 'error.php';
  include 'backend.php';


  $response = get_to_backend("/track/transaction", $_GET);

  if (!in_array($response["status_code"], array(200, 202, 424))){
    echo build_error($response,
                     "Backend error",
                     $response["status_code"]);
    return;
  }

  // Report conflict
  if (424 == $response["status_code"]){
    $body = json_decode($response["body"]);
    echo sprintf("<p>Exchange provided conflicting information about
                  transaction '%s': what claimed by the exchange does
                  not match what stored in our DB.</p>",
                  $_GET["order_id"]);
    return;
  }

  // Render HTML
  http_response_code($response["status_code"]);
  $decoded = json_decode($response["body"]);
  if (202 == $response["status_code"]){
    $pretty_date = get_pretty_date($decoded->details->execution_time);
    echo "<p>The exchange accepted the transaction.
          The exchange will attempt the payment on: $pretty_date</p>";
    return;
  }

  echo "<ul>";
  foreach ($decoded as $entry){
    $pretty_date = get_pretty_date($entry->execution_time);
    echo sprintf("<li>Wire transfer ID: %s, date: %s</li>",
                 $entry->wtid,
                 $pretty_date);
  }
  echo "</ul>";
?>
