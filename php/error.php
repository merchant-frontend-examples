<?php
  // This file is in the public domain.

  function build_error($response, $hint, $http_code){
    http_response_code($http_code);
    return json_encode(array(
                       'error' => "internal error",
                       'hint' => $hint,
                       'detail' => $response["body"]),
                       JSON_PRETTY_PRINT);
  }
?>
